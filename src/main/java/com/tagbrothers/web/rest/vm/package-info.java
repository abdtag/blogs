/**
 * View Models used by Spring MVC REST controllers.
 */
package com.tagbrothers.web.rest.vm;
